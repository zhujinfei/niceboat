﻿package com.nb.system;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.google.gson.Gson;
import com.nb.common.dao.PageVo;
import com.nb.system.model.User;
import com.nb.system.service.PageQuery;
import com.nb.system.service.UserService;

/**
 * 测试类
 * @author mf
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring.xml", "classpath:spring-mybatis.xml" })
public class UserTest {

	private static final Logger logger = Logger.getLogger(UserTest.class);
	
	private UserService userService;
	@Autowired
	public void setUserService(UserService articleService) {
		this.userService = articleService;
	}
	@Test
	public void findByCondition() {
		PageQuery query = new PageQuery();
		query.setPageNumber(1);
		query.setPageSize(3);
		Map<String, Object> params = new HashMap<String, Object>();
		query.setParams(params);
		
		logger.info(new Gson().toJson(userService.getByCondition(query)));
	}
	@Test
	public void deleteByPrimaryKey() {
		logger.info(userService.deleteByPrimaryKey(2));
	}

	@Test
	public void insert() {
		User user = new User();
		//user.setId(1);
		user.setLoginname("test");
		user.setEmail("test@163.com");
		user.setUsername("测试人员");
		user.setPhone("13912345678");
		logger.info(userService.insert(user));
	}

	@Test
	public void selectByPrimaryKey() {
		logger.info(new Gson().toJson(userService.selectByPrimaryKey(1)));
	}
	@Test
	public void updateByPrimaryKeySelective() {
		User user = new User();
		user.setId(1);
		user.setUsername("测试人员2");
		user.setLoginname(null);
		logger.info(new Gson().toJson(userService.updateByPrimaryKeySelective(user)));
	}
	@Test
	public void updateByPrimaryKey() {
		User user = new User();
		user.setId(1);
		user.setUsername("测试人员3");
		user.setLoginname(null);//只要不设值，全为空
		logger.info(new Gson().toJson(userService.updateByPrimaryKey(user)));
	}
	
	@Test
	public void getPages() {
		PageVo<User> pages = userService.getPages(1);
		logger.info(new Gson().toJson(pages));
	}
	@Test
	public void getAll() {
		List<User> users = userService.getAll();
		logger.info(new Gson().toJson(users));
	}
}
