<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/common/import-basic-js-css.jsp"%>
<script>

$(function(){
	$("#showMode").val("${ob.showMode}");

    $.metadata.setType("attr", "validate");
    var v = $("#inputForm").validate({
         //调试状态，不会提交数据的
         debug: true,
         errorPlacement: function (lable, element) {
             if (element.hasClass("l-textarea")) {
                 element.addClass("l-textarea-invalid");
             }
             else if (element.hasClass("l-text-field")) {
                 element.parent().addClass("l-text-invalid");
             }

             var nextCell = element.parents("td:first").next("td");
             nextCell.find("div.l-exclamation").remove(); 
             $('<div class="l-exclamation" title="' + lable.html() + '"></div>').appendTo(nextCell).ligerTip(); 
         },
         success: function (lable) {
             var element = $("#" + lable.attr("for"));
             var nextCell = element.parents("td:first").next("td");
             if (element.hasClass("l-textarea")) {
                 element.removeClass("l-textarea-invalid");
             }
             else if (element.hasClass("l-text-field")) {
                 element.parent().removeClass("l-text-invalid");
             }
             nextCell.find("div.l-exclamation").remove();
         },
         submitHandler: function (form) {
           form.submit();
         }
     });
    $("#inputForm").ligerForm({inputWidth:470});
    
});
</script>
</head>
<body style="background-color: #fff">

	<form id="inputForm" method="post" action="${ctx}/admin/category/save" class="form-horizontal">
	<input type="hidden" name="id" value="${ob.id }" >
		<table cellpadding="0" cellspacing="0" class="l-table-edit"  >
            <tr>
                <td align="right"  width="100px" >栏目名称:</td>
                <td align="left"  width="464px"  >
					<input type="text" class="form-control" id="name" name="name" value="${ob.name }" validate="{required:true}"  />
                </td>
                <td align="left" width="100px" ></td>
            </tr>
            <tr>
                <td align="right" >栏目url:</td>
                <td align="left"  >
					<input type="text" class="form-control" name="url" value="${ob.url }"/>
                </td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" >父id:</td>
                <td align="left"  >
					<input type="text" class="form-control" name="pid" value="${ob.pid }" />
                </td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" >显示模式:</td>
                <td align="left"  >
               		<div id="listbox1"></div> 
					<select name="showMode" id="showMode" >
						<option value="" >请选择...</option>
						<option value="2" >栏目第一条内容</option>
						<option value="1" >列表-显示文章标题</option>
						<option value="3" >列表-显示图片</option>
						<option value="3" >列表-显示下载链接</option>
					</select>
                </td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" >
					链接:</td>
                <td align="left"  >
					<input type="text" class="form-control" name="href" value="${ob.href}" maxlength="255" />
                </td>
                <td align="left"><span class="in-prompt" title="链接格式为 http://www.baidu.com "></span></td>
            </tr>
            <tr>
                <td align="right" >新窗口打开:</td>
                <td align="left"  >
               		<div id="listbox2"></div> 
					<select name="open" id="open" >
						<option value="" >请选择...</option>
						<option value="0" >否</option>
						<option value="1" >是</option>
					</select>
                </td>
                <td align="left"></td>
            </tr>
            <tr>
                <td align="right" ></td>
                <td align="left"  >
					<input type="submit" value="保存" id="Button1" class="l-button l-button-submit" /> 
                </td>
                <td align="left"></td>
            </tr>
        </table> 
	</form>
</body>	

</html>