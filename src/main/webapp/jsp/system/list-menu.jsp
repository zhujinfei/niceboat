<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="/common/import-basic-js-css.jsp"%>

<script type="text/javascript">

$(function (){
	ligergrid =  $("#maingrid").ligerGrid({
            columns: 
				[ 
					{display : '菜单名称',name : 'menuname',id:'name', width: "20%", align: 'left'}, 
					{display : 'id',name : 'id'}, 
					{display : '菜单url',name : 'url'}, 
					{display : '父级菜单ID',name : 'pid'}, 
					{display : '是否显示',name : 'sfxs'}, 
					{display : '顺序',name : 'sx'}, 
				    {
                     display: '操作', isAllowHide: false,
                     render: function (row)
                     {
                         var html ="";
                         <shiro:hasPermission name="memo:edit">
                         html+='<a href="javascript:return void(0);" onclick="f_edit({0})"  >编辑</a>&nbsp;';
                         </shiro:hasPermission >
 						<shiro:hasPermission name="memo:delete">
 						html+='<a href="javascript:return void(0);"  onclick="f_delete({0})" >删除</a>';
 						</shiro:hasPermission >
						return Free.replace(html,row.id);
                     }, width: 160
                 }

            ],
            url: '${ctx}/system/menu/ajax_list',
            pageSize: 100, sortName: 'id',
            width: '97%', height: '96%', 
            checkbox : false,
            pageParmName:'page',
            enabledSort:false, tree: {
                    columnId: 'name',
                    idField: 'id',
                    parentIDField: 'pid'
                }
        });
        
    });
function f_query(){
	ligergrid.set('parms',$('#query-form').serializeArray());
	ligergrid.loadData();
}
function f_edit(id){
  var newDialog = new Dialog({type:'iframe',value:'${ctx}/system/menu/input?id='+id},
		{title:"栏目编辑",modal:true,width:830,height:350}).show();
}

function f_add(){
	parent.newDialog({type:'iframe',value:'${ctx}/system/menu/input'},
		{title:"栏目新增",modal:true,width:830,height:350});
}
function f_delete(id){
	  freeConfirm("是否将此信息删除?",function(){
		  Free.ajax({
	         	   url: '${ctx}/system/menu/delete.do',
	         	   data: {id:id},
	         	   success: function(data){
	         		   if (data='ok'){
	         			 ligergrid.loadData();
	         		   }
		         	}
			  });
		});
}
</script>
</head>
<body>
        <table class="tab" >
          <tbody>
          <tr class="tab_white02">
            <td>     
			<form class="form-horizontal" id="query-form" method="get" action="stat-system">
					<i class="icon-search"></i>栏目名称：
						<input type="text" placeholder="模糊查询..." name="search_LIKE_menuname">
					<button type="button" onclick="f_query();" class="btn btn-primary btn-small"><i class="icon-search  icon-white"></i> 查询</button>
					<shiro:hasPermission name="menu:add">
					<button type="button" onclick="f_add();"  class="btn btn-primary btn-small"><i class="icon-plus icon-white"></i> 新增</button>
					</shiro:hasPermission>
			</form>
            </td>
          </tr>
        </tbody></table>
	<div id="maingrid"></div>
</body>	
</html>
