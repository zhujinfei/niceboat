<%@ page language="java" pageEncoding="UTF-8"%>
<%@ include file="/common/taglibs.jsp" %>  

<link href="${ctx}/ui/css/style_flat.css" rel="stylesheet">
<link href="${ctx}/ui/css/dialog.css" rel="stylesheet">

<script src="${ctx}/ui/plugins/jquery/jquery-1.10.2.min.js"></script>
<script src="${ctx}/ui/plugins/free/free.core.js"></script>
<script src="${ctx}/ui/plugins/free/free.dialog.js"></script>
<script src="${ctx}/ui/plugins/free/free.form.js"></script>

<link href="${ctx}/ui/plugins/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/ui/plugins/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/ui/plugins/ligerUI/js/ligerui.min.js"></script>

<script src="${ctx}/ui/plugins/jquery-validation/jquery.validate.min.js" type="text/javascript"></script> 
<script src="${ctx}/ui/plugins/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
<script src="${ctx}/ui/plugins/jquery-validation/messages_cn.js" type="text/javascript"></script>
    
<script src="${ctx}/static/ckeditor/ckeditor.js"></script>

<!-- zTree插件 -->
<link href="${ctx}/ui/plugins/ztree/css/zTreeStyle/zTreeStyle.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/ui/plugins/ztree/js/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="${ctx}/ui/plugins/ztree/js/jquery.ztree.excheck-3.5.js"></script>
<script type="text/javascript" src="${ctx}/ui/plugins/ztree/js/jquery.ztree.exedit-3.5.js"></script>