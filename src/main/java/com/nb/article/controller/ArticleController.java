﻿package com.nb.article.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.nutz.dao.Chain;
import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.free.common.utils.web.Servlets;
import com.nb.article.model.Article;
import com.nb.article.model.ArticleData;
import com.nb.article.service.ArticleService;
import com.nb.category.model.Category;
import com.nb.common.utils.SearchFilter;
import com.nb.system.NewPager;

/**
 * 文章控制器
 * @author 赵占涛 369880281@qq.com
 *
 */
@Controller
@RequestMapping("/admin/article")
public class ArticleController {

	/**
	 * 文章管理 
	 */
	@Autowired
	private ArticleService service;
	@Autowired
	private Dao dao;
	
	/**
	 * 添加文章
	 * @param title 标题
	 * @param content 内容
	 */
	@RequestMapping("input")
	public String input(HttpServletRequest request,
			@RequestParam(value="id",defaultValue="0",required=false) int id,
			@RequestParam(value="categoryid",defaultValue="0",required=false) int categoryid) {
		if (id>0){
			Article  article = service.fetch(id);
			request.setAttribute("ob", article);
			request.setAttribute("articleData", dao.fetch(ArticleData.class, Cnd.where("pid", "=", article.getId())));
			categoryid = article.getCategoryid();
		}
		request.setAttribute("category",dao.fetch(Category.class, Cnd.where("id","=",categoryid)));
		return "jsp/article/input-article";
	}
	
	/**
	 * 文章列表
	 */
	@RequestMapping("/list")
	public String list(HttpServletRequest request,
			@RequestParam(value="categoryid",defaultValue="0",required=false) int categoryid) {

		request.setAttribute("category",dao.fetch(Category.class, Cnd.where("id","=",categoryid)));
		return "jsp/article/list-article";
	}
	
	/**
	 * 文章列表
	 */
	@RequestMapping("/ajax_list")
	@ResponseBody
	public Map<String, Object> list(HttpServletRequest request,
			@RequestParam(value="page",defaultValue="1") int page,
			@RequestParam(value="pagesize",defaultValue="10") int pagesize) {
		
		Map<String, Object> searchParams = Servlets.getParametersStartingWith(request, "search_");
		Map<String, SearchFilter> filters = SearchFilter.parse(searchParams);
		NewPager pager = new NewPager();
		pager.setPageNumber(page);
		pager.setPageSize(pagesize);
		pager.setFilters(filters);
		
		return service.queryPage(pager);
	}
	
	/**
	 * 删除文章
	 * @param id
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id) {
		service.delete(id);
		dao.clear(ArticleData.class, Cnd.where("pid", "=", id));
		return "ok";	
	}

	/*
	 * 保存
	 */
	@RequestMapping("/save")
	public String save(Article en,ArticleData articleData,HttpServletRequest request){

		if (en.getId()==null){
			Article article = service.insert(en);
			articleData.setPid(article.getId());
			dao.insert(articleData);
		}else{
			service.updateIgnoreNull(en);
			ArticleData articledataTemp = dao.fetch(ArticleData.class, Cnd.where("pid", "=", en.getId()));
			if (articledataTemp!=null){
				dao.update(ArticleData.class,Chain.make("content", articleData.getContent()), Cnd.where("pid", "=", en.getId()));
			}else{
				articleData.setPid(en.getId());
				dao.insert(articleData);
			}
		}
		
		return "common/success";	
	}
}
