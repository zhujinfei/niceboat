﻿package com.nb.article.model;

import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Table;
/**
 * 会议文件附件
 * 模板自动生成   for FreeUI
 * @author mefly
 *
 */
@Table("t_cms_article_data") 
public class ArticleData {
	@Id
    private Integer id;
    private String content;
    private Integer pid;
    
    //-----------------------
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

  
}