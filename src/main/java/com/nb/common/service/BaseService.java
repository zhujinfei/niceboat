package com.nb.common.service;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.nutz.dao.Cnd;
import org.nutz.dao.sql.Criteria;

import com.nb.common.utils.SearchFilter;
import com.nb.common.utils.SearchFilter.Operator;
import com.nb.system.NewPager;

public class BaseService {
	
	// 拼开查询条件
	public Criteria getCriteriaFromPage(NewPager page) {
		Iterator it = page.getFilters().entrySet().iterator();
		Criteria cri = Cnd.cri();
		while (it.hasNext()) {
			Map.Entry item = (Entry) it.next(); 
			SearchFilter sf = (SearchFilter)item.getValue();
			
			String op ="=";
			if (Operator.LIKE.equals(sf.operator)){
				op="LIKE";
				cri.where().andLike(sf.fieldName,sf.value.toString(),false);
			}else{
				cri.where().and(sf.fieldName,op,sf.value.toString());
			}
		}
		if (page.DESC.equals(page.getOrder())){
			cri.getOrderBy().desc(page.getOrderBy());
		}
		if (page.ASC.equals(page.getOrder())){
			cri.getOrderBy().asc(page.getOrderBy());
		}
		return cri;
	}
}
