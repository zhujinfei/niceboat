﻿package com.nb.front.controller;

import javax.servlet.http.HttpServletRequest;

import org.nutz.dao.Cnd;
import org.nutz.dao.Dao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.nb.article.model.Article;
import com.nb.category.model.Category;
import com.nb.cms.model.Site;
import com.nb.cms.service.SiteService;

/**
 *
 */
@Controller
public class FrontController {
	@Autowired
	private Dao dao;
	@Autowired
	private SiteService siteService;
	
	private String getTheme(){
		Site site = siteService.fetchDefaultSite();
		String theme = site.getTheme();
		return  "jsp/front/"+theme;
	}
	/**
	 *
	 * @param title 标题
	 * @param content 内容
	 */
	@RequestMapping("/{url}")
	public String nav(HttpServletRequest request,
			@PathVariable("url") String url) {
		Category cate = dao.fetch(Category.class, Cnd.where("url","=",url));
		String theme = getTheme();
		if (cate!=null){
			request.setAttribute("category",cate);//当前栏目
			//列表模式
			if ("1".equals(cate.getShowMode())){
				request.setAttribute("categoryid", cate.getId());
				return theme+"/list";
			}
			//单页模式
			if ("2".equals(cate.getShowMode())){
				Article art = dao.fetch(Article.class, Cnd.where("categoryid","=",cate.getId()));
				if(art!=null){
					request.setAttribute("articleid", art.getId());
				}
				return theme+"/page_contact";
			}
	
			//列表图片
			if ("3".equals(cate.getShowMode())){
				request.setAttribute("categoryid", cate.getId());
				return theme+"/list_picture";
			}
		}
		return theme+"/"+url;
	}

	@RequestMapping("/{url}/{articleid}.html")
	public String detail(HttpServletRequest request,
			@PathVariable("url") String url,
			@PathVariable("articleid") String articleid) {

		String theme = getTheme();
		Article art = dao.fetch(Article.class, Cnd.where("id","=",articleid));
		Category cate = dao.fetch(Category.class, Cnd.where("id","=",art.getCategoryid()));
		request.setAttribute("category",cate);//当前栏目
		
		request.setAttribute("articleid", articleid);
		return theme+"/page_contact";
	}
}
