package com.nb.system.model;

import java.util.List;

import org.nutz.dao.entity.annotation.Column;
import org.nutz.dao.entity.annotation.Id;
import org.nutz.dao.entity.annotation.Many;
import org.nutz.dao.entity.annotation.Table;

/**
 * 用户信息
 * 模板自动生成   for FreeUI
 * @author mefly
 *
 */
@Table("t_system_user") 
public class User {

	@Column
	private Long deptid = null;// 所属部门ID
	@Id
	private Long id = null;// id
	@Column
	private String xm = null;// 姓名
	@Column
	private String loginname = null;// 登录名
	@Column
	private String memo = null;// 备注
	@Column
	private String mail = null;// mail
	@Column
	private String phone = null;// 手机
	@Column
	private String password = null;//密码

	@Many(target = UserRole.class, field = "userid")
    private List<UserRole> userRoles;

	//---------------------------------------
	
	
	
	public Long getDeptid() {
		return deptid;
	}

	public void setDeptid(Long deptid) {
		this.deptid = deptid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLoginname() {
		return loginname;
	}

	public void setLoginname(String loginname) {
		this.loginname = loginname;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	public List<UserRole> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	public String getXm() {
		return xm;
	}

	public void setXm(String xm) {
		this.xm = xm;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	
	
}