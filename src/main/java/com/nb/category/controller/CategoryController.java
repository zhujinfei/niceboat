﻿package com.nb.category.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.free.common.utils.web.Servlets;
import com.nb.category.model.Category;
import com.nb.category.service.CategoryService;
import com.nb.cms.service.SiteService;
import com.nb.common.utils.SearchFilter;
import com.nb.common.utils.SearchFilter.Operator;
import com.nb.system.NewPager;

/**
 * 栏目
 * @author mefly
 *
 */
@Controller
@RequestMapping("/admin/category")
public class CategoryController {

	/**
	 * 文章管理 
	 */
	@Autowired
	private CategoryService service;
	@Autowired
	private SiteService siteService;
	
	
	/**
	 * 
	 */
	@RequestMapping()
	public String tree() {
		return "jsp/category/list-category";
	}
	/**
	 * 列表
	 */
	@RequestMapping("/ajax_tree")
	@ResponseBody
	public List<Category> list(HttpServletRequest request) {
		
		return service.getAll();
	}
	
	/**
	 * 列表
	 */
	@RequestMapping("/ajax_list")
	@ResponseBody
	public Map<String, Object> list(HttpServletRequest request,
			@RequestParam(value="page",defaultValue="1") int page,
			@RequestParam(value="pagesize",defaultValue="10") int pagesize) {
		
		Map<String, Object> searchParams = Servlets.getParametersStartingWith(request, "search_");
		Map<String, SearchFilter> filters = SearchFilter.parse(searchParams);
		filters.put("siteid",new SearchFilter("siteid",Operator.EQ,siteService.fetchDefaultSite().getId()));
		NewPager pager = new NewPager();
		pager.setPageNumber(page);
		pager.setPageSize(pagesize);
		pager.setFilters(filters);
		return service.queryPage(pager);
	}

	/**
	 */
	@RequestMapping("/input")
	public String input(HttpServletRequest request,
			@RequestParam(value="id",defaultValue="0",required=false) int id) {	
		if (id>0){
			request.setAttribute("ob", service.fetch(id));
		}
		return "jsp/category/input-category";
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@RequestMapping("/delete")
	@ResponseBody
	public String delete(int id) {
		service.delete(id);
		return "ok";	
	}

	@RequestMapping("/save")
	public String save(Category en) {	
		if (en.getId()==null){
			en.setSiteid(siteService.fetchDefaultSite().getId()+"");
			service.insert(en);
		}else{
			service.updateIgnoreNull(en);
		}
		return "common/success";
	}
}
